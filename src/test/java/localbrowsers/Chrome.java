package localbrowsers;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@Test(groups = {"mac", "windows"})
public class Chrome {
    private WebDriver driver;

    @BeforeTest
    public void chromeSetup(){
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(capabilities);
    }
    
    @Test
    public void adminLogintest(){
        driver.get("https://jobprep.io");
        Assert.assertEquals(driver.getTitle(), "JobPrep Learning Management System");
        driver.findElement(By.className("nav-link:nth-child(3)")).click();
        driver.findElement(By.id("username")).sendKeys("sysadmin");
        driver.findElement(By.id("password")).sendKeys("JobPrep#21");
        driver.findElement(By.id("loginbtn")).click();
        Assert.assertEquals(driver.getTitle(), "Dashboard");
        driver.findElement(By.cssSelector("span.username")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
       // driver.findElement(By.linkText("Logout")).click();
    }
 

    @AfterTest
    public void testTeardown(){
        driver.quit();
    }
}
