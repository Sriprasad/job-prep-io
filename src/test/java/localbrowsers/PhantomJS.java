package localbrowsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by andrew on 12/3/16.
 */
@Test(groups = {"mac", "windows"})
public class PhantomJS {
    private WebDriver driver;

    @BeforeTest
    public void phantomJSSetup(){
        //DesiredCapabilities capabilities = new DesiredCapabilities.phantomjs();
        driver = new PhantomJSDriver();
    }
    @Test
    public void test(){
    	driver.get("https://jobprep.io");
        Assert.assertEquals(driver.getTitle(), "JobPrep Learning Management System");
    }

    @AfterTest
    public void testTeardown(){
        driver.quit();
    }
}
